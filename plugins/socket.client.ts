import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';
import {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR
} from '~/store/transactions';

export default ({ store }, inject) => {
  Vue.use(VueNativeSock, 'wss://ws.blockchain.info/inv', {
    store,
    mutations: {
      SOCKET_ONOPEN,
      SOCKET_ONCLOSE,
      SOCKET_ONERROR,
      SOCKET_ONMESSAGE,
      SOCKET_RECONNECT,
      SOCKET_RECONNECT_ERROR
    },
    connectManually: true,
  });
}

