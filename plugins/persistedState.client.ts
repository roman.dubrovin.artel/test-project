// @ts-ignore
import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  createPersistedState({
    key: 'app',
    paths: [
      'windows'
    ]
  })(store);
}
