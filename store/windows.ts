import Vue from 'vue';
import { WindowsState, RootState } from '~/types/state';
import { Position } from '~/types/windows';
import { MutationTree, GetterTree } from "vuex";
import config from '~/configs';

export const SET_POSITION = 'windows/SET_POSITION';
export const INIT_WINDOWS = 'windows/INIT_WINDOWS';
export const RESTORE_WINDOW = 'windows/RESTORE_WINDOW';
export const SELECT_WINDOW = 'windows/SELECT_WINDOW';

export const GET_POSITION = 'windows/GET_POSITION';
export const HAS_HIDDEN_WINDOWS = 'windows/HAS_HIDDEN_WINDOWS';

const initWindow = (zIndex): Position => ({
  x: config.gridSize,
  y: (config.windows.minHeight + config.gridSize) * zIndex,
  z: zIndex,
  width: config.windows.minWidth,
  height: config.windows.minHeight,
  visible: true
});

const getMaxZIndex = (items) => {
  return items.reduce((result: number, position: Position) => {
    result = result > position.z ? result : position.z;

    return result;
  }, 0);
};

export const state = (): WindowsState => ({
  items: []
});

export const getters: GetterTree<WindowsState, RootState> = {
  GET_POSITION: (state: WindowsState) => (index: number): Position => {
    return state.items[index];
  },
  HAS_HIDDEN_WINDOWS(state: WindowsState) {
    return state.items.some((position) => position.visible === false);
  }
};

export const mutations: MutationTree<WindowsState> = {
  SET_POSITION(state: WindowsState, { index, value }: {index: number, value: Position }): void {
    Vue.set(state.items, index, Object.assign({}, state.items[index], value));
  },

  INIT_WINDOWS(state: WindowsState): void {
    if (state.items.length >= 5) {
      return;
    }

    for (let i = 0; i < 5; i++) {
      state.items.push(initWindow(i));
    }
  },

  RESTORE_WINDOW(state: WindowsState): void {
    const hiddenWindowIndex = state.items.findIndex((position) => !position.visible);

    state.items[hiddenWindowIndex].visible = true;
    state.items[hiddenWindowIndex].x = window.innerWidth / 2 - (config.windows.minWidth);
    state.items[hiddenWindowIndex].y = window.innerHeight / 2 - (config.windows.minHeight);
    state.items[hiddenWindowIndex].width = config.windows.minWidth;
    state.items[hiddenWindowIndex].height = config.windows.minHeight;
  },

  SELECT_WINDOW(state: WindowsState, index: number): void {
    const maxZIndex = getMaxZIndex(state.items);

    state.items[index].z = maxZIndex + 1;
  }
};
