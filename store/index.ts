import { RootState } from '~/types/state';
import { MutationTree } from 'vuex';

export const state = (): RootState => ({
  title: ''
});

export const mutations: MutationTree<RootState> = {
  setTitle(state: RootState, title: string): void {
    state.title = title;
  }
};
