import Vue from 'vue';
import { TransactionsState, RootState } from '~/types/state';
import {Commit, MutationTree, ActionTree, GetterTree} from 'vuex';

export const CONNECT_SOCKET = 'transactions/CONNECT_SOCKET';
export const SUBSCRIBE_TRANSACTIONS = 'transactions/SUBSCRIBE_TRANSACTIONS';
export const UNSUBSCRIBE_TRANSACTION = 'transactions/UNSUBSCRIBE_TRANSACTION';
export const CLEAR_TRANSACTION = 'transactions/CLEAR_TRANSACTION';
export const GET_TOTAL_AMOUNT = 'transactions/GET_TOTAL_AMOUNT';

export const SOCKET_ONOPEN = 'transactions/SOCKET_ONOPEN';
export const SOCKET_ONCLOSE = 'transactions/SOCKET_ONCLOSE';
export const SOCKET_ONERROR = 'transactions/SOCKET_ONERROR';
export const SOCKET_ONMESSAGE = 'transactions/SOCKET_ONMESSAGE';
export const SOCKET_RECONNECT = 'transactions/SOCKET_RECONNECT';
export const SOCKET_RECONNECT_ERROR = 'transactions/SOCKET_RECONNECT_ERROR';

export const state = (): TransactionsState => ({
  items: [],
  socket: {
    isConnected: false,
    message: '',
    reconnectError: false,
  }
});

export const getters: GetterTree<TransactionsState, RootState> = {
  GET_TOTAL_AMOUNT(state: TransactionsState): number {
    return state.items.reduce((sum, transaction) => {
      const totalBefore = transaction.x.inputs.reduce((sum, input) => sum += input.prev_out.value, 0);
      const totalAfter = transaction.x.out.reduce((sum, out) => sum += out.value, 0);

      sum += totalBefore - totalAfter;

      return sum;
    }, 0);
  }
};

export const mutations: MutationTree<TransactionsState> = {
  SOCKET_ONOPEN(state, event): void {
    state.socket.isConnected = true
  },

  SOCKET_ONCLOSE(state: TransactionsState, event): void {
    state.socket.isConnected = false
  },

  SOCKET_ONERROR(state: TransactionsState, event): void {
    console.error(state, event)
  },

  SOCKET_ONMESSAGE(state: TransactionsState, message): void {
    state.items.unshift(JSON.parse(message.data));
  },

  SOCKET_RECONNECT(state: TransactionsState, count): void {
    console.info(state, count)
  },

  SOCKET_RECONNECT_ERROR(state: TransactionsState): void {
    state.socket.reconnectError = true;
  },

  CLEAR_TRANSACTION(state: TransactionsState): void {
    state.items = [];
  },
};

export const actions: ActionTree<TransactionsState, RootState> = {
  CONNECT_SOCKET() {
    return Vue.prototype.$connect();
  },

  SUBSCRIBE_TRANSACTIONS() {
    return Vue.prototype.$socket.send('{"op":"unconfirmed_sub"}');
  },

  UNSUBSCRIBE_TRANSACTION({ commit, state }: { commit: Commit, state: TransactionsState }) {
    return Vue.prototype.$socket.send('{"op":"unconfirmed_unsub"}');
  }
};
