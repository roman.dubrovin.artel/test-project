export declare interface SimpleTableHeaderItem {
  class: string;
  name: string;
}

export declare interface SimpleTableRowItem {
  line: string[];
}
