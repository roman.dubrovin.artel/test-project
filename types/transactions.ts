export interface TransactionRecord {
  addr: string;
  n: number;
  script: string;
  spent: boolean;
  tx_index: number;
  type: number;
  value: number;
}

export interface TransactionInputRecord {
  prev_out: TransactionRecord;
  script: string;
  sequence: number;
}

export interface Transaction {
  "op": "utx",
  "x": {
    "inputs": TransactionInputRecord[],
    "out": TransactionRecord[]
  }
}
