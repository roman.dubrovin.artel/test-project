export interface Position {
  x: number;
  y: number;
  z: number;
  width: number;
  height: number;
  visible: boolean;
}
