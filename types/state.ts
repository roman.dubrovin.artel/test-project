import {Position} from "~/types/windows";
import {Transaction} from "~/types/transactions";

export interface RootState {
  title: string;
}

export interface WindowsState {
  items: Position[];
}

export interface TransactionsState {
  items: Transaction[];
  socket: {
    isConnected: boolean;
    message: string;
    reconnectError: boolean;
  }
}
