import Vue, { ComponentOptions } from 'vue';

declare module "vue/types/options" {
  interface ComponentOptions<V extends Vue> {
    auth?: boolean;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $modal: any;
  }
}

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
