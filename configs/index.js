export default {
  gridSize: 10,
  windows: {
    minWidth: 300,
    minHeight: 100
  }
};
