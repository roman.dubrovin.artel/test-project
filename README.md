# novogradnja_admin_nuxtjs

> Nuxt.js TypeScript project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

# Используемые технологии

Использованы следующие технологии:

1. Nuxt. Это надстройка над Vue, которая позволяет осуществлять SSR. 
Это позволяет в будущем очень сильно облегчить себе работу, когда потребуется расшаривать какие-либо страницы в 
соцсетях, а так же. Упрощает SEO. 
2. i18n. Практика показывает, что переводы нужно закладывать настолько рано насколько это возможно. 
3. typescript. Позволяет писать самодокументированный код.
4. vuex - Позволяет иметь один источник истины.
5. vuex-persistedstate - требуется для того, чтоб хранить состояние vuex в локальном хранилище
6. vue-native-websocket - использовалось для подгрузки данных по websocket.
7. Sass - использовалось для более структурированого написания стилей
8. nuxt-svg-loader - мелочь, но приятно. Чтоб svg не подгружались дополнительным запросом, 
   а вставлялись в html сразу на сервере
