export default {
  sidebar: {
    dragModals: 'Drag modals',
    fetchData: 'Fetch data'
  },
  dragModalsPage: {
    title: 'Window №#{index}',
    restore: 'Restore hidden modals',
    placeholder: 'Some content'
  },
  fetchData: {
    from: 'From',
    to: 'To',
    amount: 'Amount',
    subscribe: 'Subscribe',
    unsubscribe: 'Unsubscribe',
    clear: 'Clear',
    sum: 'Total: {value}'
  },
  footer: 'Test project',
  noItemsPlaceholder: 'No data'
};
