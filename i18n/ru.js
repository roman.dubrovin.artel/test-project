export default {
  sidebar: {
    dragModals: 'Претягивание модалок',
    fetchData: 'Получение данных'
  },
  dragModalsPage: {
    title: 'Окно №{index}',
    restore: 'Восстановить скрытые окна',
    placeholder: 'Что-то внутри'
  },
  fetchData: {
    from: 'Отправитель',
    to: 'Получатель',
    amount: 'Сумма',
    subscribe: 'Запуск',
    unsubscribe: 'Остановка',
    clear: 'Сброс',
    sum: 'Сумма: {value}'
  },
  footer: 'Тестовый проект',
  noItemsPlaceholder: 'Список пуст'
};
