export default {
  env: {},
  head: {
    title: "Test project",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js TypeScript project" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.png" },
      { rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.8.1/css/all.css" }
    ]
  },
  loading: { color: "#3B8070" },
  css: [
    '~/assets/styles/index.scss'
  ],
  build: {},
  modules: [
    'nuxt-i18n',
    'nuxt-svg-loader'
  ],
  plugins: [
    '~/plugins/persistedState.client',
    '~/plugins/socket.client',
  ],
  i18n: {
    locales: [
      {
        code: 'en',
        file: 'en.js'
      },
      {
        code: 'ru',
        file: 'ru.js'
      }
    ],
    lazy: true,
    langDir: 'i18n/',
    defaultLocale: 'ru'
  }
}
